---
title: "Šampioni preferenčních hlasů: Kdo nejvíc získal, kdo nejvíc ztratil?"
perex: "Sedmačtyřicet nově zvolených krajských zastupitelů získalo mandát jen díky „kroužkování“. Jiných sedmačtyřicet kandidátů ze stejného důvodu o místo v zastupitelstvu přišlo. Podívejte se, kteří to jsou a kterými kandidátkami preferenční hlasy zamíchaly nejvíce."
description: "Sedmačtyřicet nově zvolených krajských zastupitelů získalo mandát jen díky „kroužkování“. Jiných sedmačtyřicet kandidátů ze stejného důvodu o místo v zastupitelstvu přišlo. Podívejte se, kteří to jsou a kterými kandidátkami preferenční hlasy zamíchaly nejvíce."
authors: ["Jan Boček", "Petr Kočí"]
published: "9. října 2016"
coverimg: https://interaktivni.rozhlas.cz/data/volby-2016-preference-text/www/media/cover.jpg
socialimg: https://interaktivni.rozhlas.cz/data/volby-2016-preference-text/www/media/socialimg.jpg
coverimg_note: "Josef Váňa, osminásobný vítěz Velké Pardubické. Foto Filip Jandourek, ČRo"
url: "preference-2016"
libraries: [jquery, highcharts]
recommended:
  - link: https://interaktivni.rozhlas.cz/zisky-v-krajskych-volbach/
    title: Mapa: ČSSD ztrácela napříč republikou, ANO triumfuje
    perex: Srovnání zisků jednotlivých stran v letošních krajských volbách ukazuje, že voliči odcházejí od sociálních demokratů k ANO 2011 Andreje Babiše.
    image: https://interaktivni.rozhlas.cz/zisky-v-krajskych-volbach/media/socialimg.jpg
  - link: https://interaktivni.rozhlas.cz/historie-brno/
    title: Volební mapa Brna: dominance lidovců dokáže přežít sto let a dvě diktatury
    perex: Politická paměť některých městských částí je nečekaně dlouhá. Dnešní lokální podporu křesťanských demokratů lze vystopovat až do prvních voleb za Rakouska-Uherska. Podobné vzorce platí i pro komunisty.
    image: https://interaktivni.rozhlas.cz/historie-brno/media/socimg.png
  - link: https://interaktivni.rozhlas.cz/historie-krajskych-voleb/
    title: Doleva, či doprava. Podívejte se, jak volí vaše obec
    perex: Český rozhlas připravil aplikaci, kde zjistíte, čím je vaše obec v kraji výjimečná: Které strany zde vedou, a které naopak propadají.
    image: https://interaktivni.rozhlas.cz/historie-krajskych-voleb/media/socimg.jpg
---

Ve včerejších krajských volbách rozhodli voliči o tom, co bude v dalších čtyřech letech dělat necelých sedm stovek politiků. Ne všichni ale se zvolením počítali: téměř padesát z nich se do křesla dostalo ze spodních míst kandidátek díky preferenčním hlasům.

Pro získání krajského křesla stačí, aby pět procent voličů některé kandidátní listiny zakroužkovalo konkrétního kandidáta. Při letošní slabé volební účasti stačilo na přeskočení zbytku kandidátky pár stovek hlasů: nejméně to bylo 435 preferencí, které pomohly ke zvolení v Karlovarském kraji Vojtěchu Frantovi z Pirátské strany.

<aside class="big">
  <iframe src="https://samizdat.cz/dw/oLMGF/" class="ig" width="100%" height="800" scrolling="yes" frameborder="0"></iframe>
</aside>

<aside class="big">
  <iframe src="https://samizdat.cz/dw/zmNaM/" class="ig" width="100%" height="800" scrolling="yes" frameborder="0"></iframe>
</aside>

Preferenční hlasy obvykle pomáhají těm, které voliči dobře znají: v Plzeňském kraji se z posledního místa kandidátky dostal na první miláček regionu Jiří Pospíšil z TOP 09, který ještě v minulých krajských volbách kandidoval za ODS. [Rozdílová mapa](https://interaktivni.rozhlas.cz/zisky-v-krajskych-volbach/) ukazuje jeho vliv; oproti minulým krajským volbám tady letos občanští demokraté ztratili nejvíc z celé republiky.

Kromě politických celebrit se stále častěji prosazují také silné osobnosti z jiných oblastí. V Karlovarském kraji žokej Josef Váňa přeskočil polovinu kandidátky hnutí ANO, dostal se k mandátu ze třicátého místa.

## V Plzeňském kraji preference vyřadily lídryni

Kroužkování známých osobností má ovšem dva neblahé důsledky. Jednak se tímto způsobem k politickým mandátům často dostávají ti, kteří už mají jiné funkce a na kandidátce jsou jenom pro nalákání voličů. Je u nich tedy poměrně pravděpodobné, že více funkcí stíhat nebudou. Pěkným příkladem je třeba právě Jiří Pospíšil, v současné době poslanec Evropského parlamentu a od včerejška také krajský zastupitel.

Preferenční hlasování letos také uškodilo ženám. Kroužkováním sice voliči do krajského křesla pomohli šesti ženám, zároveň ale čtrtnáct dalších odsunuli mimo ně. Při padesáti preferenčně zvolených to přitom nejsou úplně malá čísla. Preferenční volba známých tváří tedy pomáhá zachovat současný stav. U moci udržuje ty, kteří už se v politice prosadili; těmi jsou v Česku častěji muži. Doplácejí na ni naopak méně známé ženy na čele kandidátek, často je převálcují právě kolegové z konce listiny.

Přesně tohle potkalo koalici lidovců, zelených a nestraníků v Plzeňském kraji. Strana získala dvě krajská křesla, do kterých se ovšem nedostane lídryně Ivana Bartošová ani dvojka kandidátky Svatava Štěrbová. V zastupitelstvu je díky preferenčním hlasům nahradí muži z 13. a 19. místa kandidátky.

## ANO zvítězilo plošně, ale slabě

Šampioni letošních voleb – lidovci Čunek a Juránek – získali k patnácti tisícům preferenčních hlasů. V minulých krajských volbách přitom měli ti nejlepší přes dvacet tisíc hlasů. Důvody jsou dva: jednak jsou letošní výsledky poměrně vyrovnané, navzdory tmavomodré horní polovině Česka nikde ANO nezvítězilo výraznou většinou. Naopak, jak zmiňuje [IHNED](http://domaci.ihned.cz/c1-65470090-deset-hlavnich-zprav-z-voleb), jde o nejméně výrazné vítězství od vzniku krajů. Hlasy voličů se tříští mezi spoustu stran, proto je i preferenčních hlasů pro jednotlivé kandidáty méně.

<aside class="big">
  <iframe src="https://interaktivni.rozhlas.cz/data/volby-2016-preference-text/charts/preference2016.html" class="ig" width="100%" height="400" scrolling="no" frameborder="0"></iframe>
</aside>

<aside class="big">
  <iframe src="https://interaktivni.rozhlas.cz/data/volby-2016-preference-text/charts/preference2012.html" class="ig" width="100%" height="400" scrolling="no" frameborder="0"></iframe>
</aside>

Druhým důvodem pro celkově méně preferencí je fakt, že voliči nejsilnějšího hnutí ANO kandidáty prakticky nekroužkovali (výjimkou je snad jen zmíněný žokej Váňa). Kandidátky ANO zůstaly po volbách téměř beze změny; koho strana na první místa posadila, ten tam obvykle zůstal. ANO ve většině krajů nevítězí pomocí silných regionálních kandidátů. Letos ostatně do lokální politiky přivedlo spoustu nováčků, které voliči zatím z politiky neznají, a tedy nekroužkují.

Velké téma letošních voleb je také neúspěch dosavadního hejtmana Michala Haška z ČSSD, ten se z 20 tisíc preferencí před čtyřmi lety propadl na letošních osm tisíc.